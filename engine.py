import datetime
import json
import os
import re
import uuid

import boto3
from tornado import gen
from wand.image import Image
import requests
from bs4 import BeautifulSoup
from slugify import slugify
import feedparser

import settings

"""
#
# LAUNCH PARSER
#
"""
"""
def launch(id):
    base_site = BaseSite.objects.get(id=id)
    parser(base_site)
"""


class ParserSite(object):

    def __init__(self, id, rss, connection):
        self.id = id
        self.base_site_rss = rss
        self.link_id = None
        self.connection = connection
        self.path = "/opt/contterfeedparsing/media/{}".format(str(id))

    def run(self):
        self.init_client()
        self.feedparser()

    def init_client(self):
        self.client_s3 = boto3.client(
            's3',
            aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
            aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)

    @staticmethod
    def get_time_from_feed(entry):
        created = datetime.datetime.utcnow()
        try:
            created = datetime.datetime(*entry.published_parsed[:6])
        except:
            pass

        return created

    @gen.coroutine
    def feedparser(self):
        print('fffff', self)
        feed = feedparser.parse(self.base_site_rss)
        if not feed.entries:
            return

        cursor = yield self.connection.execute(
                            "SELECT * FROM base_link WHERE url IN (" + ','.join(["'"+entry.link.replace("'", "''")+"'" for entry in feed.entries]) + ")")

        diff = [f.url for f in cursor.fetchall()]
        cursor.close()
        diff = list(set([f.link for f in feed.entries]) - set(diff))
        print('len', len(feed.entries), len(diff))
        for entry in feed.entries:
            if entry.link in diff:
                created = self.get_time_from_feed(entry)
                base_link = self.create_link(entry, created)
                print('base_link', base_link)
                if base_link:
                    self.send_user_link(base_link)

    def send_user_link(self, base_link):
        """
        Отправляем по sns
        :param base_link:
        :return:
        """
        self.client_sns = boto3.client('sns', region_name='us-west-2',
                           aws_access_key_id=settings.AWS_ACCESS_KEY_ID,
                           aws_secret_access_key=settings.AWS_SECRET_ACCESS_KEY)

        response = self.client_sns.publish(
            TopicArn='arn:aws:sns:us-west-2:034672374092:createuserlink',
            Message=json.dumps({"link_id": self.link_id, "site_id", self.id}),
            Subject='create_link'
        )
        print(response)

    @staticmethod
    def grouplen(sequence, chunk_size):
        return list(zip(*[iter(sequence)] * chunk_size))

    @gen.coroutine
    def create_link(self, entry, created):
        """
        Создаем ссылку
        :param entry:
        :param created:
        :return:
        """
        request = requests.get(entry['link'], headers=self.get_request_headers())
        soup = BeautifulSoup(request.text, "html.parser")

        title = None
        try:
            try:
                title = soup.find("title").text
            finally:
                title = soup.find("meta", {"property": "og:title"})['content']
        except:
            pass

        description = ''
        try:
            try:
                description = soup.find("meta", {"name": "description"})['content']
            finally:
                description = soup.find("meta", {"property": "og:description"})['content']
        except:
            pass

        keywords = ''
        try:
            try:
                keywords = soup.find("meta", {"name": "keywords"})['content']
            finally:
                keywords = soup.find("meta", {"property": "og:keywords"})['content']
        except:
            pass

        img = None
        try:
            try:
                img = soup.find("meta", {"property": "og:image"})['content']
            finally:
                img = soup.find("meta", {"name": "image"})['content']
        except:
            pass

        filename, filename_thumb, json_img = None, None, None
        if img:
            try:
                filename, filename_thumb, json_img = self.create_image(title, img)
            except Exception as e:
                print('image', e)

        cursor = yield self.connection.execute(
                        "INSERT INTO base_link (is_active, title, description, keywords, url, full_url, medium_img, thumbnail, created, json_img) VALUES ({0}, '{1}', '{2}', '{3}', '{4}','{5}', '{6}', '{7}', '{8}', '{9}') RETURNING *;".format(
                            True, title.replace("'", "''"), description.replace("'", "''"),
                            keywords.replace("'", "''"), entry.link, entry.link, filename, filename_thumb, created, json.dumps(json_img)))
        item = cursor.fetchone()
        print(item.id)
        if item:
            self.link_id = int(item.id)
            return int(item.id)

        return

    def create_image(self, title, img):
        json_img = {}
        r = requests.get(img, headers=self.get_request_headers())
        name = slugify(title)
        headers = r.headers.get('content-type', 'image/jpeg')

        result = re.findall(r'(image/png)|(image/jpeg)|(image/gif)', headers)
        res = ''.join(result[0])
        mime = re.findall(r'/\w*', res)[0].replace('/', '')
        img_name = '{}_image.{}'.format(name, mime)
        img_thumb_name = '{}_image__thumb.{}'.format(name, mime)
        path_img = self.create_s3_path(img_name)
        path_img_thumb = self.create_s3_path(img_thumb_name)

        filename, short_name = self.create_unique_name_file(img_name)
        f = open(filename, 'wb')
        f.write(r.content)
        f.close()

        with open(filename, 'rb') as data:
            self.client_s3.upload_fileobj(data, 'contter', path_img, ExtraArgs={'ContentType': res})

        filename_thumb, short_thumb_name = self.create_unique_name_file(img_thumb_name)
        with Image(filename=filename) as img:
            json_img["medium_img"] = {"height": img.height, "width": img.width}
            img.transform(resize='27x')
            img.format = mime
            img.save(filename=filename_thumb)

        with open(filename_thumb, 'rb') as data:
            self.client_s3.upload_fileobj(data, 'contter', path_img_thumb, ExtraArgs={'ContentType': res})

        os.remove(filename)
        os.remove(filename_thumb)

        return path_img, path_img_thumb, json_img

    def create_s3_path(self, name):

        return '{}/{}/{}'.format(datetime.datetime.utcnow().strftime("%Y%m%d"), str(self.id), name)

    def create_unique_name_file(self, unique_name):
        """
        Создаем уникальное имя для файла.
        :param path: путь до папки с файлами.
        :param name: название файла.
        :return:
        """
        if not os.path.exists(self.path):
            os.makedirs(self.path)
        f = os.path.join(self.path, unique_name)
        if os.path.isfile(f):
            unique_name = str(uuid.uuid4())[:12] + "__" + unique_name
            return self.create_unique_name_file(unique_name)
        return f, unique_name

    def get_request_headers(self):
        return {
            "Accept-Language": "en-US,en;q=0.5",
            "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.28 Safari/537.36",
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
            "Referer": "https://www.reddit.com",
            "Connection": "keep-alive"
        }
